import 'react-native-gesture-handler';

import React from 'react';
import { View, Text, StyleSheet, SafeAreaView} from 'react-native';
import Navigation from './src/Navigation'

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
        <Navigation/>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    fontFamily: "Poppins-Medium"
  },
});

export default App;
