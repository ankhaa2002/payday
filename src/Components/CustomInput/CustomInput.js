import React from "react";
import {View, Text, StyleSheet, Image, TextInput} from 'react-native';
import {Controller} from 'react-hook-form';

const CustomInput = ({control, name, placeholder, src, secureTextEntry, rules}) => {
    return(
        <Controller
            control={control}
            name={name}
            rules={rules}
            render={({field: {value, onChange, onBlur}, fieldState: {error}})=> (
                <>
                    <View style={[styles.container, {borderColor : error ? 'red' : '#d9d9d9'}]}>
                    <Image
                        style={styles.icon}
                        source={src}
                        resizeMode="contain"
                    />
                        <TextInput
                            style={styles.input}
                            placeholder={placeholder}
                            value={value}
                            onChangeText={onChange}
                            onBlur={onBlur}
                            secureTextEntry={secureTextEntry}
                        />
                    </View>
                    {error && ( 
                    <Text style={{color: 'red', alignSelf: 'stretch', fontFamily: "Poppins-Regular"}}>{error.message || Error}</Text>
                    )}
                </>
            )}
        />
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 50,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: "#D9D9D9",
        paddingHorizontal: 16,
        backgroundColor: "#fff",
        margin:8,
        flexDirection: 'row',
        color: "#7d7d7d"
    },
    icon: {
        width: 16,
        height: 16,
        marginTop: 16,
    },
    input: {
        width: '100%',
        marginLeft: 16,
        fontFamily: "Poppins-Medium"
    }
})

export default CustomInput;