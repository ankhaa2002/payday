import React from "react";
import {View, Text, StyleSheet, Image, Pressable} from 'react-native';

const CustomContainer = ({Name, Detail, onPress, type = "PRIMARY"}) => {
    return(
        <Pressable onPress={onPress} style={[styles.container, styles[`container_${type}`]]}>
            <Text style={[styles.input, styles[`input_${type}`]]}>{Name}</Text>
            <Text style={[styles.details, styles[`details_${type}`]]}>{Detail}</Text>
        </Pressable>
    );
};

const styles = StyleSheet.create({
    container_PRIMARY: {
        width: '100%',
        height: 84,
        borderRadius: 13,
        borderWidth: 3,
        borderColor: '#F3F3F3',
        color: "#000",
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    details_PRIMARY: {
        fontFamily: "Poppins-Regular",
        fontSize: 10,
        color: "#000"
    },
    input_PRIMARY: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 16,
        color: "#000"
    },
    container_SECONDARY: {
        width: '100%',
        height: 84,
        color: "#000",
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    details_SECONDARY: {
        fontFamily: "Poppins-Regular",
        fontSize: 10,
        color: "#6E6E6E"
    },
    input_SECONDARY: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 16,
        color: "#6E6E6E"
    },
    container_ACTIVE: {
        width: '100%',
        height: 84,
        borderLeftWidth: 3,
        borderLeftColor: '#000',
        color: "#000",
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    details_ACTIVE: {
        fontFamily: "Poppins-Regular",
        fontSize: 10,
        color: "#000"
    },
    input_ACTIVE: {
        fontFamily: "Poppins-SemiBold",
        fontSize: 16,
        color: "#000"
    },
    container_PROMO: {
        width: '100%',
        height: 84,
        borderWidth: 3,
        borderRadius: 13,
        borderColor: '#d7d7d7',
        paddingHorizontal: 10,
        paddingVertical: 10,
        alignItems: 'center'
    },
    details_PROMO: {
        textAlign: 'center',
        fontFamily: "Poppins-Regular",
        fontSize: 10,
        color: "#000"
    },
    input_PROMO: {
        textDecorationLine: 'underline',
        fontFamily: "Poppins-SemiBold",
        fontSize: 16,
        color: "#000"
    },
})

export default CustomContainer;