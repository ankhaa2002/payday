import React from "react";
import {View, Text, StyleSheet, Image, Pressable} from 'react-native';

const CustomInput = ({onPress, btText, type = "PRIMARY"}) => {
    return(
        <Pressable onPress={onPress} style={[styles.container, styles[`container_${type}`]]}>
            <Text style={[styles.text, styles[`text_${type}`]]}>{btText}</Text>
        </Pressable>
    );
};

const styles = StyleSheet.create({
    container: {
        width: 362,
        height: 50,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container_PRIMARY: {
        backgroundColor: "#000",
    },
    container_SECONDARY: {
        backgroundColor: "#fff"
    },
    text: {
        fontFamily: "Poppins-Medium",
        color: 'white'
    },
    text_SECONDARY: {
        fontFamily: "Poppins-Medium",
        color: "#7D7D7D"
    }
})

export default CustomInput;