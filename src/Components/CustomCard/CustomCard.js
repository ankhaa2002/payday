import React from "react";
import {StyleSheet, Image, View, Text} from 'react-native';
import Redbutton from '../CustomRedButton'
import Greybutton from '../CustomGreyButton'
import Profile from '../../../Assets/Images/img-user.png'


const CustomCard = () => {
    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <View>
                    <Image
                        style={styles.pro}
                        source={Profile}
                        resizeMode="contain"
                    />
                </View>
                <View style={styles.Title}>
                    <Text style={styles.Name} >Ankhbileg Binderiya</Text>
                    <Text style={styles.class}>Software Engineering - 4</Text>
                </View>
            </View>
            <View>
                <Text style={styles.bio}>Adventure enthusiast 🌍 | Foodie 🍔 | Dog lover 🐶 | Let's create some memories together!</Text>
            </View>
            <View style={styles.section}>
                <View style={styles.action} >
                    <Greybutton onPress={console.log("UnLike")} style={styles.button}/>
                    <Redbutton onPress={console.log("Like")} style={styles.button}/>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        maxWidth: '100%',
        maxHeight: '100%',
        borderRadius: 16,
        borderColor: '#E4E4E4',
        borderWidth: 2,
        paddingHorizontal: 20,
        backgroundColor: "#FFF",
        paddingHorizontal: 20,
        paddingVertical: 16,
        marginVertical: 8,
    },
    action: {
        marginTop: 16,
        flexDirection: 'row',
    },
    button: {
        marginHorizontal:4,
    },
    header:{
        flexDirection: 'row',
        marginBottom: 8,
    },
    pro: {
        width: 40,
        height: 40,
        marginLeft: 4,
    },
    Title: {
        marginLeft: 8,
    },
    Name: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#000'
    },
    class: {
        color: '#515151'
    },
    bio: {
        color: '#515151'
    },
    section: {
        alignItems: 'center',
    }
})

export default CustomCard;