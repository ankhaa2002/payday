import React from "react";
import {StyleSheet, Image, Pressable} from 'react-native';
import Heart from '../../../Assets/Icons/heart2.png'

const CustomRedButton = ({onPress}) => {
    return(
        <Pressable onPress={onPress} style={styles.container}>
            <Image source={Heart} style={styles.Heart}/>
        </Pressable>
    );
};

const styles = StyleSheet.create({
    container: {
        margin: 4,
        width: 160,
        height: 40,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#E75B4E",
    },
    Heart: {
        width: 16,
        height: 16,
        resizeMode: "contain",
    }
})

export default CustomRedButton;