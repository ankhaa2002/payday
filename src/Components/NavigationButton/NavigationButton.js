import React from 'react';
import {Image, StyleSheet, Pressable, View, Text} from 'react-native';
import { useNavigation } from '@react-navigation/native';

const NavigationButton = () => {

    const navigation = useNavigation();
    
    const Activity = () =>{
        navigation.navigate('Activity');
    }
    const Account = () =>{
        navigation.navigate('Account');
    }
    const Services = () =>{
        navigation.navigate('Services');
    }
    return(
        <View style={styles.footer}>
            <View style={styles.root}>
                <Pressable onPress={Activity}><Image style={styles.Item} source={require('../../../Assets/Icons/activity.png')} /></Pressable>
                <Text>Activity</Text>
            </View>
            <View style={styles.root}>
                <Pressable onPress={Services}><Image style={styles.Item} source={require('../../../Assets/Icons/cube.png')} /></Pressable>
                <Text>Services</Text>
            </View>         
            <View style={styles.root}>
            <Pressable onPress={Account}><Image style={styles.Item} source={require('../../../Assets/Icons/user.png')} /></Pressable>
                <Text>Account</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    footer: {
        borderTopWidth: 2,
        borderColor: '#E4E4E4',
        width: 432,
        height: 95,
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        position: 'absolute',
        paddingHorizontal: 65,
        bottom: 0,
        backgroundColor: '#fff'
    },
    Item: {
        height: 19,
        width: 19,
        resizeMode: 'stretch',
        marginBottom: 11,
    },
    root: {
        marginTop: 15,
        alignItems: 'center'
    }
})

export default NavigationButton;