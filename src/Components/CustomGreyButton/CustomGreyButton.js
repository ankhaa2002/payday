import React from "react";
import {StyleSheet, Image, Pressable} from 'react-native';
import Cross from '../../../Assets/Icons/cross2.png'

const CustomGreyButton = ({onPress}) => {
    return(
        <Pressable onPress={onPress} style={styles.container}>
            <Image source={Cross} style={styles.Cross}/>
        </Pressable>
    );
};

const styles = StyleSheet.create({
    container: {
        margin: 4,
        width: 160,
        height: 40,
        borderRadius: 16,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#E4E4E4",
    },
    Cross: {
        width: 16,
        height: 16,
        resizeMode: "contain",
    }
})

export default CustomGreyButton;