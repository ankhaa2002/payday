import React from "react";
import {View, Text, StyleSheet, Image, Pressable} from 'react-native';

const CustomTittle = ({tittle}) => {
    return(
        <View style={styles.container}>
            <Text style={styles.txt}>{tittle}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        
    },
    txt: {
        fontFamily: 'Cunia',
        fontSize: 25,
        color: '#000'
    },
})

export default CustomTittle;