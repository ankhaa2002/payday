import React, { useState } from "react";
import {View, Text, StyleSheet, Image} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {useForm, Controller,} from 'react-hook-form';

import Logo from '../../../Assets/Images/logo.png'
import Input from '../../Components/CustomInput'
import Button from '../../Components/CustomBigButton'
import user from '../../../Assets/Icons/user.png'
import lock from '../../../Assets/Icons/lock.png'

const Login = () => {

    const EMAIL_REGEX = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/

    const {control, handleSubmit, formState: {errors}} = useForm();

    console.log(errors);

    const navigation = useNavigation();

    const onLoginPressed = data =>{
        console.log(data)
        navigation.navigate('Search')
    }

    return (
        <View style={styles.root}>
            <Image source={Logo} style={styles.logo} resizeMode="contain"/>
            <Input
                src={user}
                placeholder="Email"
                name="Email"
                control={control}
                rules={{
                    required: 'Email is required',
                    pattern: {value: EMAIL_REGEX, message: 'Email is invalid'}
                }}
            />
            <Input
                src={lock}
                placeholder="Password"
                name="Password"
                control={control}
                secureTextEntry={true}
                rules={{required: 'Password is required', minLength: {
                    value: 8, 
                    message: 'Password should be minimum 8 characters long'
                }}}
            />
            <View style={styles.btn}>
                <Button
                    btText={"Login"}
                    onPress={handleSubmit(onLoginPressed)}
                />
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        padding: 35,
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
    },
    logo: { 
        width: 115,
        height: 115,
        marginTop: 125,
        marginBottom: 80,
    },
    btn: {
        position: 'absolute',
        flex: 1, 
        top: 700,
    }
})
export default Login;