import React, { useState } from "react";
import {View, Text, StyleSheet, Pressable, Image} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import Tittle from '../../Components/CustomTittle'
import Container from '../../Components/CustomContainer'
import Frame  from '../../Components/CustomFrame'
import CutsomButton from '../../Components/CustomButton'

import Shield from '../../../Assets/Icons/shield.png'
import Card from '../../../Assets/Icons/credit-card.png'
import Phone from '../../../Assets/Icons/smartphone.png'
import Key from '../../../Assets/Icons/key.png'
import Friend from '../../../Assets/Icons/friend.png'
import Info from '../../../Assets/Icons/info.png'
import Time from '../../../Assets/Icons/time.png'
import Network from '../../../Assets/Icons/network.png'
import App from '../../../Assets/Icons/App.png'
import Close from '../../../Assets/Icons/close.png'
import Notfi from '../../../Assets/Icons/bell2.png'

const Settings = () => {

    const navigation = useNavigation();
    
    const onLogOutPressed = () =>{
        navigation.navigate('Login');
    }
    const BackOnPressed = () =>{
        navigation.navigate('Account');
    }
    return (
        <View style={styles.root}>
            <View style={styles.cont}>
                <Tittle tittle="Settings"/>
                <Pressable onPress={BackOnPressed}>
                    <Image source={Close} style={styles.back} resizeMode="contain"/>
                </Pressable>
            </View>
            <Text>General</Text>
            <View style={styles.options}>
                <Frame type={"THIRD"} Name='Appearance' src={App}/>
                <Frame type={"THIRD"} Name='Connections' src={Network} />
                <Frame type={"THIRD"} Name='Time Zones' src={Time} />
                <Frame type={"THIRD"} Name='About' src={Info}/>
                <Frame type={"THIRD"} Name='Notfications' src={Notfi}/>
            </View>
            <Text>My Team</Text>
            <View style={styles.options}>
                <Frame type={"THIRD"} Name='User Managment' src={Friend}/>
                <Frame type={"THIRD"} Name='Permission' src={Key} />
                <Frame type={"THIRD"} Name='Authentication' src={Phone} />
                <Frame type={"THIRD"} Name='Payments' src={Card} />
                <Frame type={"THIRD"} Name='Security & Access' src={Shield} />
            </View>
            <CutsomButton
                btText={"Log Out"}
                onPress={onLogOutPressed}
            />
        </View>
    );
};
const styles = StyleSheet.create({
    root: {
        flex: 1,
        padding: 35,
        backgroundColor: '#f3f3f3'
    },
    cont: {
        flexDirection: 'row',
        justifyContent:'space-between',
        marginBottom: 20,
    },
    section: {
        marginTop: 16,
        fontFamily: "Poppins-SemiBold",
        fontSize: 20,
        color: "#000" 
    },
    upcoming: {
        marginTop: 16,
        backgroundColor: '#fff'
    },
    data: {
        marginBottom: 16,
    },
    back: {
        width: 16,
        height: 16,
        marginTop: 2
    },
    options: {
        paddingVertical: 0,
        marginTop: 20,
        marginBottom: 20,
    }
})
export default Settings;