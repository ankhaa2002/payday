import React, { useState } from "react";
import {View, Text, StyleSheet, Pressable, Image} from 'react-native';
import { useNavigation } from '@react-navigation/native';

import Tittle from '../../Components/CustomTittle'
import Container from '../../Components/CustomContainer'

import Close from '../../../Assets/Icons/close.png'

const Messages = () => {

    const navigation = useNavigation();
    
    const HistoryOnPressed = () =>{
        navigation.navigate('History');
    }
    const BackOnPressed = () =>{
        navigation.navigate('Account');
    }
    return (
        <View style={styles.root}>
            <View style={styles.cont}>
                <Tittle tittle="MESSAGES"/>
                <Pressable onPress={BackOnPressed}>
                    <Image source={Close} style={styles.back} resizeMode="contain"/>
                </Pressable>
            </View>
            <View style={styles.upcoming}>
                <Container type={"ACTIVE"} Name='Your balance' Detail='Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus volutpat fringilla .'/>
                <Container type={"SECONDARY"} Name='Give away' Detail='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit .'/>
                <Container type={"ACTIVE"} Name='Your balance' Detail=' Aliquam ac commodo dui, laoreet luctus nunc. Mauris non ante vitae turpis vulputate aliquam.'/>
                <Container type={"SECONDARY"} Name='Your balance' Detail='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit .'/>
                <Container type={"SECONDARY"} Name='Give away' Detail='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit .'/>
                <Container type={"SECONDARY"} Name='Your task' Detail='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit .'/>
                <Container type={"SECONDARY"} Name='Give away' Detail='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit .'/>
                <Container type={"SECONDARY"} Name='Your task' Detail='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit .'/>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    root: {
        flex: 1,
        padding: 35,
        backgroundColor: '#fff'
    },
    cont: {
        flexDirection: 'row',
        justifyContent:'space-between'
    },
    section: {
        marginTop: 16,
        fontFamily: "Poppins-SemiBold",
        fontSize: 20,
        color: "#000" 
    },
    upcoming: {
        marginTop: 16,
        backgroundColor: '#fff'
    },
    data: {
        marginBottom: 16,
    },
    back: {
        width: 16,
        height: 16,
        marginTop: 2
    }
})
export default Messages;